# Model DTO

from pydantic import BaseModel
from typing import List
# from typing import Union

class Crayon(BaseModel):
    crayon_id: int
    couleur: str

class Trousse(BaseModel):
    trousse_id: int
    contient: List[Crayon]

class Crayon_dans_trousse(Crayon):
    contenu: Trousse

# # Example usage:
# crayon_data = {"crayon_id": 1, "couleur": "blue"}
# trousse_data = {"trousse_id": 1, "contient": [crayon_data]}

# crayon_instance = Crayon(**crayon_data)
# trousse_instance = Trousse(**trousse_data)

# # Create a CrayonInTrousse instance
# crayon_in_trousse = CrayonInTrousse(**crayon_data, trousse=trousse_instance)

