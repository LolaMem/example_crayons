
DROP DATABASE IF EXISTS crayons;
CREATE DATABASE crayons;
use crayons;

CREATE TABLE trousse (
    trousse_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
);

CREATE TABLE crayon (
    crayon_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    couleur VARCHAR(30)
);

CREATE TABLE trousse_crayon (
    crayon_id INT REFERENCES crayon(crayon_id),
    trousse_id INT REFERENCES trousse(trousse_id),
    PRIMARY KEY(crayon_id, trousse_id)
);

DROP USER IF EXISTS lolam1@localhost;
CREATE USER lolam1@localhost IDENTIFIED BY 'lolam1';
GRANT ALL PRIVILEGES ON crayons.* to lolam1@localhost;
