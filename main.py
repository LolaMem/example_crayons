from fastapi import FastAPI
import mysql.connector
from dto import Crayon, Trousse, Crayon_dans_trousse
from typing import List

mydb = mysql.connector.connect(
    host="localhost",
    user="lolam1",
    password="lolam1",
    database="crayons"
)

# uvicorn main:app --reload

app = FastAPI(debug=True)

#@app.post("/crayon/{crayon_id}")
def create_crayon(couleur: str) -> int:
    """créer un nouveau crayon à partir de sa couleur"""
    with mydb.cursor(buffered=True) as cursor:
        cursor.execute("""
            insert into crayon(couleur) values (%s);
        """, [couleur])
        mydb.commit()
    return cursor.lastrowid

#@app.get("/crayon")
# def get_all_crayon() -> list:
#     cursor = mydb.cursor(buffered=True)
#     cursor.execute("""SELECT * FROM crayon""")
#     list_crayons = cursor.fetchall()
#     return list_crayons

def get_all_crayon() -> List:
    with mydb.cursor(buffered=True) as cursor:
        cursor.execute("""SELECT * FROM crayon""")
        list_crayons = cursor.fetchall()
    return list_crayons


#@app.get('/crayon/{crayon_id}')
def get_crayon_by_id(crayon_id: int):
    with mydb.cursor(dictionary=True, buffered=True) as cursor:
        cursor.execute("SELECT * FROM crayon WHERE crayon_id = %s", [crayon_id])
        result = cursor.fetchone()
        #query = "SELECT * FROM crayon WHERE crayon_id = %s"
    return result

#@app.delete("/crayon")
def delete_crayon(crayon_id: int):
    cursor = mydb.cursor()
    cursor.execute("""
        DELETE FROM crayon WHERE crayon_id = %s;
    """, [crayon_id])
    mydb.commit()
    return True

#@app.put("/crayon/{couleur}")
def update_crayon(couleur: str, crayon_id: int):
    cursor = mydb.cursor(buffered=True)
    cursor.execute("""UPDATE crayon SET couleur = %s WHERE crayon_id = %s;""", 
        [couleur, crayon_id])
    mydb.commit()
    return True


#@app.post("/trousse")
def create_trousse():
    cursor = mydb.cursor()
    cursor.execute("""INSERT INTO trousse VALUES ();""")
    mydb.commit()
    return cursor.lastrowid

# insert a crayon dans un trousse
def crayon_dans_trousse(trousse_id: int, crayon_id: int) -> List:
    cursor = mydb.cursor()
    cursor.execute("""
        INSERT INTO trousse_crayon (trousse_id, crayon_id) VALUES (%s, %s);
    """, [trousse_id,crayon_id])
    mydb.commit()
    list_crayon_et_trousse = cursor.fetchall()
    return list_crayon_et_trousse

### Max: get all trousse with content:
# Get all the trousses with their content
@app.get("/trousse")
def get_all_trousse():
    cursor = mydb.cursor(dictionary=True, buffered=True)
    cursor.execute("""select * from trousse;""")
    trousses = cursor.fetchall()
    for trousse in trousses:
        cursor.execute("""
        select crayon.crayon_id, crayon.couleur from crayon
        join trousse_crayon on crayon.crayon_id = trousse_crayon.crayon_id
        where trousse_crayon.trousse_id = %s;
        """, [trousse["trousse_id"]])
        trousse["contient"] = cursor.fetchall()
    return trousses

# Get all the trousses with their content
@app.put("/mettre/{crayon_id}/{trousse_id}")
def mettre_crayon_trousse(crayon_id: int, trousse_id: int):
    cursor = mydb.cursor()
    cursor.execute("""
        delete from trousse_crayon where crayon_id = %s;
    """, [crayon_id])
    cursor.execute("""
        insert into trousse_crayon(trousse_id, crayon_id) 
        VALUES (%s, %s);
    """, [trousse_id, crayon_id])
    mydb.commit()
    return True








if __name__ == '__main__':
    print("test créer un crayon")
    r = create_crayon('violet')
    print(r)

    print("test recup la liste des crayons")
    l = get_all_crayon()
    print(l)

    print("test recup le crayon avec l'id")
    c = get_crayon_by_id(r)
    print(c)

    print("changer la couleur du crayon pour rouge foncé")
    update_crayon("rouge foncé", r)
    print(l)
    #print(get_crayon_by_id(r))

    print("supprimer le crayon")
    delete_crayon(r)
    print(get_all_crayon())

    print("creer un trousse")
    c = create_trousse()
    print(c)

    print("mettre un crayon dans un trousse")
    d = mettre_crayon_trousse(1,1)
    print(d)

    print("show all trousses and the content")
    f = get_all_trousse()
    print(f)

