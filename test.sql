
SOURCE init.sql

INSERT INTO crayon (couleur) values ("rouge");
INSERT INTO crayon (couleur) values ("blanc");
INSERT INTO crayon (couleur) values ("vert");
INSERT INTO crayon (couleur) values ("bleu");
INSERT INTO crayon (couleur) values ("orange");
INSERT INTO crayon (couleur) values ("noir");
SELECT * FROM crayon;

INSERT INTO trousse values ();
INSERT INTO trousse values ();
INSERT INTO trousse values ();
SELECT * FROM trousse;

INSERT INTO trousse_crayon (trousse_id, crayon_id) VALUES
    (1, 1),
    (1, 2),
    (2, 3),
    (2, 4),
    (3, 5),
    (3, 6);

-- comment trouver dans quel trousse est un crayon
select * FROM crayon
JOIN trousse_crayon on crayon.crayon_id = trousse_crayon.crayon_id
WHERE crayon.crayon_id = 1;

-- savoir tous les crayons qui sont dans une trousse
SELECT * FROM crayon
JOIN trousse_crayon on crayon.crayon_id = trousse_crayon.crayon_id
WHERE trousse_id = 1;

-- trouver des crayons par couleur et dans quel trousse ils sont
SELECT * FROM crayon
JOIN trousse_crayon on crayon.crayon_id = trousse_crayon.crayon_id
JOIN trousse on trousse_crayon.trousse_id = trousse.trousse_id
WHERE crayon.couleur LIKE "%vert%";  